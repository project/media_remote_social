# Media Remote Social

A simple module that extends oEmbed support added to Drupal Core's Media module
by implementing hook_media_source_info_alter() for the following providers:
- Facebook, Instagram, X, TikTok, Pinterest


## Requirements

Drupal Core Media (media) module.


## Installation

`composer require drupal/media_remote_social:^1.0`


## Configuration

This module does not have configuration.


## Features

- **Media type:** Creates a 'Remote social' Media type
- **Manage fields:** Creates a 'Social URL' (field_media_oembed_social) plain
  text field
- **Manage form display:** Uses the 'oEmbed URL' widget
- **Manage display:** Uses the 'oEmbed content' widget


## Usage:

To add Remote social content, simply go to `/media/add/remote_social`
and paste the social url from Facebook, Instagram, X, TikTok, Pinterest.


## Facebook & Instragram:

See: https://developers.facebook.com/docs/features-reference/oembed-read
- oEmbed Page: https://developers.facebook.com/docs/graph-api/reference/oembed-page/
- oEmbed Post: https://developers.facebook.com/docs/graph-api/reference/oembed-post/
- oEmbed Video: https://developers.facebook.com/docs/graph-api/reference/oembed-video/
- Instagram oEmbed: https://developers.facebook.com/docs/graph-api/reference/instagram-oembed/
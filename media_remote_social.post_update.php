<?php

/**
 * @file
 * Contains post-update hooks for Media Remote Social.
 */

/**
 * Ensures the remote social media source has the correct metadata attributes.
 */
function media_remote_social_post_update_fix_thumbnail_metadata_attributes() {
  // Deliberately empty in order to force a container rebuild.
}
